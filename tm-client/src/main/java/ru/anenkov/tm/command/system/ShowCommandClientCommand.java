package ru.anenkov.tm.command.system;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommandClient;

import java.util.Collection;

public class ShowCommandClientCommand extends AbstractCommandClient {

    @Override
    public @Nullable String arg() {
        return "-c";
    }

    @Override
    public @Nullable String name() {
        return "Commands";
    }

    @Override
    public @Nullable String description() {
        return "View commands";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[COMMANDS]");
        @Nullable final Collection<AbstractCommandClient> commandClients = bootstrap.viewCommands();
        int index = 1;
        for (AbstractCommandClient abstractCommandClient : commandClients) {
            System.out.println(index + ".\t" + abstractCommandClient.name());
            index++;
        }
        System.out.println("[SUCCESS]");
    }

}
