package ru.anenkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.endpoint.IAdminUserEndpoint;
import ru.anenkov.tm.api.service.IServiceLocator;
import ru.anenkov.tm.entity.Session;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.enumeration.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AdminUserEndpoint extends AdminEndpoint implements IAdminUserEndpoint {

    public AdminUserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    public AdminUserEndpoint() {
        super(null);
    }

    @WebMethod
    @Override
    public void lockUserByLogin(
            @NotNull @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "login") String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().lockUserByLogin(login);
    }

    @WebMethod
    @Override
    public void unlockUserByLogin(
            @NotNull @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "login") String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().unlockUserByLogin(login);
    }

    @WebMethod
    @Override
    public void deleteUserByLogin(
            @NotNull @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "login") String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().deleteUserByLogin(login);
    }

    @WebMethod
    @Override
    @NotNull
    public User removeUser(
            @NotNull @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "user") User user
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().removeUser(user);
    }

    @WebMethod
    @Override
    @NotNull
    public User removeByIdUser(
            @NotNull @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "id") String id
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().removeById(id);
    }

    @WebMethod
    @Override
    @NotNull
    public User removeByLoginUser(
            @NotNull @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "login") String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().removeByLogin(login);
    }

    @WebMethod
    @Override
    @NotNull
    public User removeByEmailUser(
            @NotNull @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "email") String email
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().removeByEmail(email);
    }

}
