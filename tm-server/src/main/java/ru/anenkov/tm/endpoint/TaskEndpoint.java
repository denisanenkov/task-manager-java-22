package ru.anenkov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.endpoint.ITaskEndpoint;
import ru.anenkov.tm.api.service.IServiceLocator;
import ru.anenkov.tm.api.service.ITaskService;
import ru.anenkov.tm.entity.Session;
import ru.anenkov.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    public TaskEndpoint() {
        super(null);
    }

    @WebMethod
    @SneakyThrows
    public void create(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "name") final String name,
            @Nullable @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().create(session.getUserId(), name, description);
    }

    @WebMethod
    @SneakyThrows
    public void add(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "task") final Task task
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().add(session.getUserId(), task);
    }

    @WebMethod
    @SneakyThrows
    public void remove(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "task") final Task task
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().remove(session.getUserId(), task);
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public List<Task> findAll(
            @Nullable @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

    @WebMethod
    @SneakyThrows
    public void clear(
            @Nullable @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().clear(session.getUserId());
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public Task findOneByIndex(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOneByIndex(session.getUserId(), index);
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public Task findOneByName(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOneByName(session.getUserId(), name);
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public Task findOneById(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOneById(session.getUserId(), id);
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public Task removeOneByIndex(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeOneByIndex(session.getUserId(), index);
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public Task removeOneByName(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeOneByName(session.getUserId(), name);
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public Task removeOneById(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeOneById(session.getUserId(), id);
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public Task updateTaskById(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "id") final String id,
            @Nullable @WebParam(name = "name") final String name,
            @Nullable @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().updateTaskById(session.getUserId(), id, name, description);
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public Task updateTaskByIndex(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "index") final Integer index,
            @Nullable @WebParam(name = "name") final String name,
            @Nullable @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().updateTaskByIndex(session.getUserId(), index, name, description);
    }

    @WebMethod
    public void load(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "task") final List tasks
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().load(tasks);
    }

    @WebMethod
    @Nullable
    public List<Task> getList(
            @Nullable @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().getList();
    }

}
