package ru.anenkov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.endpoint.IAdminEndpoint;
import ru.anenkov.tm.api.service.IServiceLocator;
import ru.anenkov.tm.entity.Session;
import ru.anenkov.tm.enumeration.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    public AdminEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    public AdminEndpoint() {
        super(null);
    }

    @SneakyThrows
    @WebMethod
    public void saveDataBinary(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().saveDataBinary();

    }

    @SneakyThrows
    @WebMethod
    public void loadDataBinary(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().loadDataBinary();
    }

    @SneakyThrows
    @WebMethod
    public void clearDataBinary(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().clearDataBinary();
    }

    @SneakyThrows
    @WebMethod
    public void saveDataBase64(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().saveDataBase64();
    }

    @SneakyThrows
    @WebMethod
    public void loadDataBase64(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().loadDataBase64();
    }

    @SneakyThrows
    @WebMethod
    public void clearDataBase64(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().clearDataBase64();
    }

    @SneakyThrows
    @WebMethod
    public void saveDataXml(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().saveDataXML();
    }

    @SneakyThrows
    @WebMethod
    public void loadDataXml(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().loadDataXML();
    }

    @SneakyThrows
    @WebMethod
    public void clearDataXml(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().clearDataXML();
    }

    @SneakyThrows
    @WebMethod
    public void saveDataJson(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().saveDataJson();
    }

    @SneakyThrows
    @WebMethod
    public void loadDataJson(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().loadDataJson();
    }

    @SneakyThrows
    @WebMethod
    public void cleanDataJson(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDataService().clearDataJson();
    }

}
